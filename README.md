[![pipeline status](https://gitlab.com/wpdesk/fedex-pro-shipping-service/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/fedex-pro-shipping-service/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/fedex-pro-shipping-service/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/fedex-pro-shipping-service/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/fedex-pro-shipping-service/v/stable)](https://packagist.org/packages/wpdesk/fedex-pro-shipping-service) 
[![Total Downloads](https://poser.pugx.org/wpdesk/fedex-pro-shipping-service/downloads)](https://packagist.org/packages/wpdesk/fedex-pro-shipping-service) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/fedex-pro-shipping-service/v/unstable)](https://packagist.org/packages/wpdesk/fedex-pro-shipping-service) 
[![License](https://poser.pugx.org/wpdesk/fedex-pro-shipping-service/license)](https://packagist.org/packages/wpdesk/fedex-pro-shipping-service) 

FedEx PRO Shipping Service
==========================

