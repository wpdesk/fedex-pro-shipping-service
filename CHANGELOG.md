## [2.3.0] - 2022-07-26
### Added
- Packer settings decorator

## [2.2.1] - 2022-04-12
### Changed
- Method settings header

## [2.2.0] - 2022-04-11
### Added
- Support for shipping zones

## [2.1.6] - 2022-02-08
### Changed
- One Rate service texts

## [2.1.0] - 2022-01-07
### Added
- One Rate service

## [2.0.0] - 2021-08-30
### Changed
- Upgrading composer libraries

## [1.2.0] - 2021-01-13
### Added
- blackout lead days

## [1.1.6] - 2020-03-03
### Fixed
- days in transit calculation
- cutoff time when lead time is zero
- custom services table location in settings 

## [1.1.5] - 2020-03-03
### Added
- Dates and time feature

## [1.1.4] - 2020-02-10
### Fixed
- translations

## [1.1.3] - 2020-02-07
### Added
- translations

## [1.1.0] - 2020-02-07
### Added
- custom origin feature

## [1.0.0] - 2020-02-05
### Added
- initial version
