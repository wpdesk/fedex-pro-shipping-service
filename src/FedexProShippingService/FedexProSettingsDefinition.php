<?php
/**
 * Settings definition.
 *
 * @package WPDesk\FedexProShippingService;
 */

namespace WPDesk\FedexProShippingService;


use WPDesk\AbstractShipping\Exception\SettingsFieldNotExistsException;
use WPDesk\AbstractShipping\Settings\SettingsDecorators\BlackoutLeadDaysSettingsDefinitionDecoratorFactory;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\FedexProShippingService\CustomOrigin\CustomOriginSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\CustomOrigin\HeaderSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\CutoffTime\CutoffTimeSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\DatesAndTimes\DatesAndTimesSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\EstimatedDelivery\EstimatedDeliverySettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\HandlingFees\HandlingFeesSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\LeadTime\LeadTimeSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\MaximumTransitTime\MaximumTransitTimeSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\OneRate\OneRateSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\PackagingType\PackagingTypeSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\PackingMethod\PackingMethodSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\RateCurrency\RateCurrencySettingsDefinitionDecorator;
use WPDesk\FedexShippingService\FedexSettingsDefinition;

/**
 * Settings definitions.
 */
class FedexProSettingsDefinition extends SettingsDefinition {

	/**
	 * Fedex settings definition.
	 *
	 * @var SettingsDefinition
	 */
	private $fedex_settings_definition;

	/**
	 * FedexProSettingsDefinition constructor.
	 *
	 * @param SettingsDefinition $fedex_settings_definition Fedex settings definition.
	 * @param string $locale ,
	 */
	public function __construct( SettingsDefinition $fedex_settings_definition, $locale ) {
		$fedex_settings_definition = new HandlingFeesSettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new RateCurrencySettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new CustomOriginSettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new DatesAndTimesSettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new EstimatedDeliverySettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new MaximumTransitTimeSettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new LeadTimeSettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new CutoffTimeSettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = ( new BlackoutLeadDaysSettingsDefinitionDecoratorFactory() )->create_decorator( $fedex_settings_definition, CutoffTimeSettingsDefinitionDecorator::OPTION_CUTOFF_TIME, false );
		$fedex_settings_definition = new PackagingTypeSettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new ShippingMethodTypeDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new OneRateSettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new PackingMethodSettingsDefinitionDecorator( $fedex_settings_definition, $locale );
		$fedex_settings_definition = new HeaderSettingsDefinitionDecorator( $fedex_settings_definition, $locale );

		$this->fedex_settings_definition = $fedex_settings_definition;
	}

	/**
	 * Get form fields.
	 *
	 * @return array
	 *
	 * @throws SettingsFieldNotExistsException .
	 */
	public function get_form_fields() {
		return $this->fedex_settings_definition->get_form_fields();
	}

	/**
	 * Validate settings.
	 *
	 * @param SettingsValues $settings Settings.
	 *
	 * @return bool
	 */
	public function validate_settings( SettingsValues $settings ) {
		return $this->fedex_settings_definition->validate_settings( $settings );
	}

}
