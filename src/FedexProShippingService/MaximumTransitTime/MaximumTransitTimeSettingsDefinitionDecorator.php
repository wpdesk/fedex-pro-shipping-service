<?php

/**
 * Decorator for maximum transit time.
 *
 * @package WPDesk\FedexProShippingService\MaximumTransitTime
 */

namespace WPDesk\FedexProShippingService\MaximumTransitTime;

use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierAfter;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\FedexProShippingService\EstimatedDelivery\EstimatedDeliverySettingsDefinitionDecorator;
use WPDesk\FedexShippingService\FedexSettingsDefinition;

/**
 * Can decorate settings for maximum transit time field.
 */
class MaximumTransitTimeSettingsDefinitionDecorator extends SettingsDefinitionModifierAfter {
	const OPTION_MAXIMUM_TRANSIT_TIME = 'maximum_transit_time';

	public function __construct( SettingsDefinition $fedex_settings_definition ) {
		parent::__construct( $fedex_settings_definition, EstimatedDeliverySettingsDefinitionDecorator::OPTION_DELIVERY_DATES, self::OPTION_MAXIMUM_TRANSIT_TIME, array(
			'title'             => \__( 'Maximum Time in Transit', 'fedex-pro-shipping-service' ),
			'type'              => 'number',
			'description'       => \__( 'Maximum Time in Transit is used to define the number of maximum days goods can be in transit. Only days in transit are counted. This is often used for perishable goods.', 'fedex-pro-shipping-service' ),
			'desc_tip'          => \true,
			'custom_attributes' => array( 'min' => '0' )
		) );
	}
}
