<?php

/**
 * Decorator for estimated delivery settings.
 *
 * @package WPDesk\FedexProShippingService\DestinationAddressType
 */

namespace WPDesk\FedexProShippingService\EstimatedDelivery;

use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierAfter;
use WPDesk\FedexProShippingService\DatesAndTimes\DatesAndTimesSettingsDefinitionDecorator;

/**
 * Can decorate settings for estimated delivery field.
 */
class EstimatedDeliverySettingsDefinitionDecorator extends SettingsDefinitionModifierAfter {
	const OPTION_DELIVERY_DATES = 'delivery_dates';
	const OPTION_NONE = 'none';
	const OPTION_DELIVERY_DATE = 'delivery_date';
	const OPTION_DAYS_TO_ARRIVAL_DATE = 'days_to_arrival_date';

	public function __construct( \WPDesk\AbstractShipping\Settings\SettingsDefinition $fedex_settings_definition ) {
		parent::__construct( $fedex_settings_definition, DatesAndTimesSettingsDefinitionDecorator::DATES_AND_TIMES_TITLE,
			self::OPTION_DELIVERY_DATES, array(
				'title'       => \__( 'Estimated Delivery', 'fedex-pro-shipping-service' ),
				'type'        => 'select',
				'options'     => array(
					self::OPTION_NONE                 => \__( 'None', 'fedex-pro-shipping-service' ),
					self::OPTION_DAYS_TO_ARRIVAL_DATE => \__( 'Show estimated days to delivery date', 'fedex-pro-shipping-service' ),
					self::OPTION_DELIVERY_DATE        => \__( 'Show estimated delivery date', 'fedex-pro-shipping-service' )
				),
				'description' => \__( 'You can show customers an estimated delivery date or time in transit. The information will be added to the service name in the checkout.', 'fedex-pro-shipping-service' ),
				'desc_tip'    => \true,
				'default'     => self::OPTION_NONE
			) );
	}
}
