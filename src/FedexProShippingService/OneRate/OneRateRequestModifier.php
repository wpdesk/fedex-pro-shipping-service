<?php
/**
 * Request modifier for One Rate.
 *
 * @package WPDesk\FedexProShippingService\OneRate
 */

namespace WPDesk\FedexProShippingService\OneRate;

use FedEx\RateService\ComplexType\RateRequest;
use FedEx\RateService\SimpleType\ServiceOptionType;
use WPDesk\FedexProShippingService\FedexApi\FedexRateRequestModifier;

/**
 * Can modify request for One Rate.
 */
class OneRateRequestModifier implements FedexRateRequestModifier {

	/**
	 * One Rate setting.
	 *
	 * @var string
	 */
	private $one_rate;

	/**
	 * .
	 *
	 * @param string $one_rate .
	 */
	public function __construct( $one_rate ) {
		$this->one_rate = $one_rate;
	}

	/**
	 * Modify rate request.
	 *
	 * @param RateRequest $request
	 */
	public function modify_rate_request( RateRequest $request ) {
		if ( 'yes' === $this->one_rate ) {
			$request->setVariableOptions( [ ServiceOptionType::_FEDEX_ONE_RATE ] );
		}
	}

}
