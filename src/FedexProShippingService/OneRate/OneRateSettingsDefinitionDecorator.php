<?php
/**
 * Decorator for one rate.
 *
 * @package WPDesk\FedexProShippingService\OneRate
 */

namespace WPDesk\FedexProShippingService\OneRate;

use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierAfter;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\FedexShippingService\FedexSettingsDefinition;

/**
 * Can decorate settings for One Rate field.
 */
class OneRateSettingsDefinitionDecorator extends SettingsDefinitionModifierAfter {

	const OPTION_ONE_RATE = 'one_rate';

	const OPTION_ONE_RATE_DEFAULT = 'no';

	public function __construct( SettingsDefinition $fedex_settings_definition ) {
		parent::__construct(
			$fedex_settings_definition,
			FedexSettingsDefinition::FIELD_DESTINATION_ADDRESS_TYPE,
			self::OPTION_ONE_RATE,
			array(
				'title'       => __( 'One Rate Pricing', 'fedex-pro-shipping-service' ),
				'type'        => 'checkbox',
				'label'       => __( 'Enable to use One Rate pricing', 'fedex-pro-shipping-service' ),
				'description' => __( 'Tick this checkbox in order to use the FedEx One Rate pricing instead of standard or account-specific rates. Option available only for U.S. domestic shipments based on sender\'s and recipient\'s addresses.', 'fedex-pro-shipping-service' ),
				'desc_tip'    => true,
				'default'     => self::OPTION_ONE_RATE_DEFAULT,
			)
		);
	}

}
