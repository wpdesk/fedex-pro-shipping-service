<?php
/**
 * Fedex API: Build request.
 *
 * @package WPDesk\FedexProShippingService\FedexApi
 */

namespace WPDesk\FedexProShippingService\FedexApi;

use FedEx\RateService\ComplexType\RateRequest;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\AbstractShipping\Shipment\Shipment;
use WPDesk\AbstractShipping\Shop\ShopSettings;
use WPDesk\FedexProShippingService\EstimatedDelivery\EstimatedDeliveryRequestModifier;
use WPDesk\FedexProShippingService\EstimatedDelivery\EstimatedDeliverySettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\OneRate\OneRateRequestModifier;
use WPDesk\FedexProShippingService\OneRate\OneRateSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\PackagingType\PackagingTypeRequestModifier;
use WPDesk\FedexProShippingService\PackagingType\PackagingTypeSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\RateCurrency\RateCurrencyRequestModifier;
use WPDesk\FedexProShippingService\RateCurrency\RateCurrencySettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\ShipTimestamp\ShipTimestamp;
use WPDesk\FedexProShippingService\ShipTimestamp\ShipTimestampRequestModifier;
use WPDesk\FedexShippingService\FedexApi\FedexRateRequestBuilder;

/**
 * Build request for Fedex rate
 */
class FedexProRateRequestBuilder extends FedexRateRequestBuilder {

	/**
	 * @var FedexRateRequestModifier[]
	 */
	private $rate_request_modifiers = array();

	/**
	 * Settings values.
	 *
	 * @var SettingsValues
	 */
	private $settings;

	/**
	 * WooCommerce shipment.
	 *
	 * @var Shipment
	 */
	private $shipment;

	/**
	 * Shop settings.
	 *
	 * @var ShopSettings
	 */
	private $shop_settings;

	/**
	 * @var ShipTimestamp
	 */
	private $ship_timestamp;

	/**
	 * FedexProRateRequestBuilder constructor.
	 *
	 * @param SettingsValues $settings Settings.
	 * @param Shipment       $shipment Shipment.
	 * @param ShopSettings   $shop_settings Helper.
	 * @param ShipTimestamp  $ship_timestamp
	 */
	public function __construct( SettingsValues $settings, Shipment $shipment, ShopSettings $shop_settings, ShipTimestamp $ship_timestamp ) {
		parent::__construct( $settings, $shipment, $shop_settings );
		$this->settings       = $settings;
		$this->shipment       = $shipment;
		$this->shop_settings  = $shop_settings;
		$this->ship_timestamp = $ship_timestamp;
		$this->init_modifiers();
	}

	/**
	 * Init modifiers.
	 */
	private function init_modifiers() {
		$rate_currency = $this->settings->get_value(
			RateCurrencySettingsDefinitionDecorator::OPTION_RATE_CURRENCY,
			RateCurrencySettingsDefinitionDecorator::OPTION_RATE_CURRENCY_DEFAULT
		);
		$delivery_dates = $this->settings->get_value(
			EstimatedDeliverySettingsDefinitionDecorator::OPTION_DELIVERY_DATES,
			EstimatedDeliverySettingsDefinitionDecorator::OPTION_NONE
		);
		$one_rate = $this->settings->get_value(
			OneRateSettingsDefinitionDecorator::OPTION_ONE_RATE,
			OneRateSettingsDefinitionDecorator::OPTION_ONE_RATE_DEFAULT
		);

		$packing_method = $this->settings->get_value(
			'packing_method',
			'weight'
		);

		$packaging_type = $this->settings->get_value(
			PackagingTypeSettingsDefinitionDecorator::OPTION_PACKAGING_TYPE,
			PackagingTypeSettingsDefinitionDecorator::OPTION_PACKAGING_TYPE_DEFAULT
		);

		$this->rate_request_modifiers[] = new RateCurrencyRequestModifier( $rate_currency, $this->shop_settings->get_default_currency() );

		if ( EstimatedDeliverySettingsDefinitionDecorator::OPTION_NONE !== $delivery_dates ) {
			$this->rate_request_modifiers[] = new EstimatedDeliveryRequestModifier( $delivery_dates, $this->shipment );
			$this->rate_request_modifiers[] = new ShipTimestampRequestModifier( $this->ship_timestamp );
		}

		$this->rate_request_modifiers[] = new OneRateRequestModifier( $one_rate );

		$this->rate_request_modifiers[] = new PackagingTypeRequestModifier( $packaging_type, $packing_method );
	}

	/**
	 * Build request.
	 *
	 * @return RateRequest
	 */
	public function build_request() {
		$request = parent::build_request();
		foreach ( $this->rate_request_modifiers as $modifier ) {
			$modifier->modify_rate_request( $request );
		}
		return $request;
	}

}
