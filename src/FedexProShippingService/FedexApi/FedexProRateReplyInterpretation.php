<?php
/**
 * FedEx rate reply.
 *
 * @package WPDesk\FedexProShippingService\FedexApi
 */

namespace WPDesk\FedexProShippingService\FedexApi;

use FedEx\RateService\ComplexType\RatedShipmentDetail;
use FedEx\RateService\ComplexType\RateReply;
use FedEx\RateService\ComplexType\RateReplyDetail;
use WPDesk\AbstractShipping\Rate\SingleRate;
use WPDesk\FedexProShippingService\ShipTimestamp\ShipTimestamp;
use WPDesk\FedexShippingService\FedexApi\FedexRateReplyInterpretation;

/**
 * Can interpret FedEx response as SingleRate.
 */
class FedexProRateReplyInterpretation extends FedexRateReplyInterpretation {

	/**
	 * @var ShipTimestamp
	 */
	private $ship_timestamp;

	/**
	 * FedexProRateReplyInterpretation constructor.
	 *
	 * @param RateReply $reply Rate reply.
	 * @param bool $is_tax_enabled Is tax enabled.
	 * @param string $rate_type Setting value of FedexSettingsDefinition::FIELD_REQUEST_TYPE
	 * @param ShipTimestamp $ship_timestamp .
	 */
	public function __construct( RateReply $reply, $is_tax_enabled, $rate_type, ShipTimestamp $ship_timestamp ) {
		parent::__construct( $reply, $is_tax_enabled, $rate_type );
		$this->ship_timestamp = $ship_timestamp;
	}
	/**
	 * Get single rate.
	 *
	 * @param RatedShipmentDetail $rated_shipment_detail .
	 * @param RateReplyDetail $reply_detail .
	 *
	 * @return SingleRate
	 *
	 * @throws \Exception .
	 */
	protected function get_single_rate( RatedShipmentDetail $rated_shipment_detail, RateReplyDetail $reply_detail ) {
		$rate = parent::get_single_rate( $rated_shipment_detail, $reply_detail );

		$this->add_delivery_date_to_rate_if_exists( $rate, $rated_shipment_detail, $reply_detail );

		return $rate;
	}

	/**
 	 * Add delivery date ( if exists ) to rate.
	 *
	 * @param SingleRate $rate
	 * @param RatedShipmentDetail $rated_shipment_detail
	 * @param RateReplyDetail $reply_detail .
	 *
	 * @throws \Exception
	 */
	private function add_delivery_date_to_rate_if_exists( SingleRate $rate, RatedShipmentDetail $rated_shipment_detail, RateReplyDetail $reply_detail ) {
		if ( isset( $reply_detail->DeliveryTimestamp ) ) {
			$rate->delivery_date = date_create_from_format( 'Y-m-d\TH:i:s', $reply_detail->DeliveryTimestamp )->setTime( 0, 0, 0, 0 );
			$ship_timestamp_as_date = new \DateTime();
			$ship_timestamp_as_date->setTimestamp( $this->ship_timestamp->calculate_ship_timestamp() )->setTime( 0, 0, 0, 0 );
			$rate->business_days_in_transit = $rate->delivery_date->diff( $ship_timestamp_as_date )->format( '%a' );
		}
	}

}