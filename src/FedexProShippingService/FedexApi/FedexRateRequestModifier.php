<?php
/**
 * Rate request modifier.
 *
 * @package WPDesk\FedexProShippingService\FedexApi
 */

namespace WPDesk\FedexProShippingService\FedexApi;

use FedEx\RateService\ComplexType\RateRequest;

/**
 * Interface for Fedex rate modifiers.
 */
interface FedexRateRequestModifier {

	/**
	 * Modify rate request.
	 *
	 * @param RateRequest $request
	 */
	public function modify_rate_request( RateRequest $request );

}