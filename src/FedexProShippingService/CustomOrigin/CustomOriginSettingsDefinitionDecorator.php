<?php
/**
 * Decorator for custom origin settings field.
 *
 * @package WPDesk\FedexProShippingService\CustomOrigin
 */

namespace WPDesk\FedexProShippingService\CustomOrigin;

use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierAfter;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\FedexShippingService\FedexSettingsDefinition;

/**
 * Can decorate settings by adding handling fees field.
 */
class CustomOriginSettingsDefinitionDecorator extends SettingsDefinitionModifierAfter {

	const CUSTOM_ORIGIN = 'custom_origin';

	const FIELD_TYPE = 'custom_origin';

	public function __construct( SettingsDefinition $fedex_settings_definition ) {
		parent::__construct(
			$fedex_settings_definition,
			FedexSettingsDefinition::FIELD_SERVICES_TABLE,
			self::CUSTOM_ORIGIN,
			array(
				'type' => self::FIELD_TYPE,
			)
		);
	}

}