<?php

namespace WPDesk\FedexProShippingService\Exception;

use WPDesk\AbstractShipping\Shop\ShopSettings;

/**
 * Exception thrown when there is no rates in shop currency.
 *
 * @package WPDesk\FedexProShippingService\Exception
 */
class ProNoRatesInCurrencyInRatingsException extends \RuntimeException {

	/**
	 * @param ShopSettings $shop_settings .
	 */
	public function __construct( ShopSettings $shop_settings ) {
		$settings_url = admin_url( 'admin.php?page=wc-settings&tab=shipping&section=flexible_shipping_fedex' );
		$message = sprintf(
			__( 'The shop\'s currency is other than set on the FedEx account. Go to %1$splugin settings%2$s and select the Rate Currency option to receive the rates in the shop\'s currency.', 'fedex-pro-shipping-service' ),
			'<a href="' . $settings_url . '" target="_blank">',
			'</a>'
		);
		parent::__construct( $message );
	}

}

