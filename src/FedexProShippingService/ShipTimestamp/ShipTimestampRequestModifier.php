<?php
/**
 * Request modifier for ship timestamp.
 *
 * @package WPDesk\FedexProShippingService\ShipTimestamp
 */

namespace WPDesk\FedexProShippingService\ShipTimestamp;

use FedEx\RateService\ComplexType\RateRequest;
use WPDesk\FedexProShippingService\FedexApi\FedexRateRequestModifier;

/**
 * Can modify request for lead time.
 */
class ShipTimestampRequestModifier implements FedexRateRequestModifier {

	/**
	 * @var ShipTimestamp
	 */
	private $ship_timestamp;

	/**
	 * DestinationAddressTypeRequestModifier constructor.
	 *
	 * @param ShipTimestamp $ship_timestamp .
	 */
	public function __construct( ShipTimestamp $ship_timestamp ) {
		$this->ship_timestamp = $ship_timestamp;
	}

	/**
	 * Modify rate request.
	 *
	 * @param RateRequest $request
	 */
	public function modify_rate_request( RateRequest $request ) {
		$ship_timestamp = $this->ship_timestamp->calculate_formatted_ship_timestamp();
		if ( '' !== $ship_timestamp ) {
			$request->RequestedShipment->setShipTimestamp( $ship_timestamp );
		}
	}

}