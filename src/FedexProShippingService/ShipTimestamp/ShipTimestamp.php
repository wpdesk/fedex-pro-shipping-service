<?php
/**
 * Ship Timestamp.
 *
 * @package WPDesk\FedexProShippingService\ShipTimestamp;
 */

namespace WPDesk\FedexProShippingService\ShipTimestamp;

use WPDesk\AbstractShipping\Settings\BlackoutLeadDays;
use WPDesk\AbstractShipping\Settings\SettingsDecorators\BlackoutLeadDaysSettingsDefinitionDecoratorFactory;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\FedexProShippingService\CutoffTime\CutoffTimeSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\EstimatedDelivery\EstimatedDeliverySettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\LeadTime\LeadTimeSettingsDefinitionDecorator;

/**
 * Can calculate ship time from lead time or/and cutoff time.
 */
class ShipTimestamp {

	const SHIP_TIMESTAMP_FORMAT = 'Y-m-d\TH:i:s';

	/**
	 * @var string
	 */
	private $delivery_dates;

	/**
	 * @var string
	 */
	private $lead_time;

	/**
	 * @var string
	 */
	private $cutoff_time;

	/**
	 * @var int
	 */
	private $current_timestamp;

	/**
	 * @var BlackoutLeadDays
	 */
	private $blackout_lead_days;

	/**
	 * ShipTimestamp constructor.
	 *
	 * @param string           $delivery_dates Delivery dates setting.
	 * @param string           $lead_time Lead time setting.
	 * @param string           $cutoff_time Cutoff time setting.
	 * @param BlackoutLeadDays $blackout_lead_days .
	 */
	public function __construct( $delivery_dates, $lead_time, $cutoff_time, $blackout_lead_days ) {
		$this->delivery_dates     = $delivery_dates;
		$this->lead_time          = $lead_time;
		$this->cutoff_time        = $cutoff_time;
		$this->current_timestamp  = current_time( 'timestamp' );
		$this->blackout_lead_days = $blackout_lead_days;
	}

	/**
	 * Create from settings.
	 *
	 * @param SettingsValues $settings
	 *
	 * @return ShipTimestamp
	 */
	public static function create_from_settings( SettingsValues $settings ) {
		$delivery_dates = $settings->get_value(
			EstimatedDeliverySettingsDefinitionDecorator::OPTION_DELIVERY_DATES,
			EstimatedDeliverySettingsDefinitionDecorator::OPTION_NONE
		);
		$lead_time = (int) $settings->get_value(
			LeadTimeSettingsDefinitionDecorator::OPTION_LEAD_TIME,
			'0'
		);
		$cutoff_time = $settings->get_value(
			CutoffTimeSettingsDefinitionDecorator::OPTION_CUTOFF_TIME,
			''
		);
		$blackout_lead_days_settings = $settings->get_value( BlackoutLeadDaysSettingsDefinitionDecoratorFactory::OPTION_ID, '' );
		$blackout_lead_days = new BlackoutLeadDays( is_array( $blackout_lead_days_settings ) ? $blackout_lead_days_settings : array(), $lead_time );
		return new ShipTimestamp( $delivery_dates, $lead_time, $cutoff_time, $blackout_lead_days );
	}

	/**
	 * Calculate ship timestamp;
	 */
	public function calculate_ship_timestamp() {
		$ship_timestamp = $this->current_timestamp;
		$calculated_date = ( new \DateTime() )->setTimestamp( $this->current_timestamp );
		if ( EstimatedDeliverySettingsDefinitionDecorator::OPTION_NONE !== $this->delivery_dates ) {
			if ( 0 === $this->lead_time ) {
				if ( '' !== $this->cutoff_time ) {
					if ( intval( date( 'H', $this->current_timestamp ) >= intval( $this->cutoff_time ) ) ) {
						$ship_timestamp  += DAY_IN_SECONDS;
						$calculated_date = ( new \DateTime() )->setTimestamp( $ship_timestamp );
						$calculated_date = $this->blackout_lead_days->calculate_date( $calculated_date );
					}
				}
			}
			$calculated_date = $this->blackout_lead_days->calculate_date( $calculated_date );
			$ship_timestamp  = $calculated_date->getTimestamp();
		}
		return $ship_timestamp;
	}

	/**
	 * Calculate formatted ship timestamp;
	 */
	public function calculate_formatted_ship_timestamp() {
		return $this->format_ship_timestamp_from_time( $this->calculate_ship_timestamp() );
	}

	/**
	 * Create ship timestamp from time.
	 *
	 * @param int $time
	 *
	 * @return string
	 */
	private function format_ship_timestamp_from_time( $time ) {
		return 0 === $time ? '' : date( self::SHIP_TIMESTAMP_FORMAT, $time );
	}

}