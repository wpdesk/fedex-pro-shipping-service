<?php

namespace WPDesk\FedexProShippingService;

use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\FedexShippingService\FedexSettingsDefinition;

/**
 * Can change shipping method type settings.
 */
class ShippingMethodTypeDecorator extends SettingsDefinition {

	const ENABLE_SHIPPING_METHOD_DEFAULT = 'no';

	/**
	 * @var SettingsDefinition
	 */
	private $settings;

	/**
	 * @param SettingsDefinition $settings
	 */
	public function __construct( SettingsDefinition $settings ) {
		$this->settings = $settings;
	}

	public function get_form_fields() {
		$form_fields = $this->settings->get_form_fields();

		$form_fields[ FedexSettingsDefinition::ENABLE_SHIPPING_METHOD ] = [
			'title'       => __( 'Shipping Method Type', 'fedex-pro-shipping-service' ),
			'type'        => 'select',
			'options'     => [
				'no'  => __( 'Standard shipping methods', 'fedex-pro-shipping-service' ),
				'yes' => __( 'Global shipping method', 'fedex-pro-shipping-service' ),
			],
			'description' => sprintf(
				__( 'Select the %1$sStandard shipping methods%2$s if you want to add the \'FedEx Live Rates\' shipping method within specific shipping zones or choose the %1$sGlobal shipping method%2$s to enable the FedEx Live Rates for all the shipping zones in your store at once.', 'fedex-pro-shipping-service' ),
				'<strong>',
				'</strong>'
			),
			'desc_tip'    => false,
			'default'     => self::ENABLE_SHIPPING_METHOD_DEFAULT,
		];

		return $form_fields;
	}

	/**
	 * Validate settings.
	 *
	 * @param SettingsValues $settings Settings values.
	 *
	 * @return bool
	 */
	public function validate_settings( SettingsValues $settings ) {
		return $this->settings->validate_settings( $settings );
	}

}
