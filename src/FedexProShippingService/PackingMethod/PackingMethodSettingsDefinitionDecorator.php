<?php

namespace WPDesk\FedexProShippingService\PackingMethod;

use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierAfter;
use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierBefore;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\FedexProShippingService\OneRate\OneRateSettingsDefinitionDecorator;
use WPDesk\WooCommerceShippingPro\Packer\PackerSettings;

/**
 * Can decorate settings by adding packing method field.
 */
class PackingMethodSettingsDefinitionDecorator extends SettingsDefinitionModifierAfter {

	/**
	 * Decorated settings definition.
	 *
	 * @var SettingsDefinition
	 */
	private $decorated_settings_definition;

	/**
	 * @var string
	 */
	private $locale;

	/**
	 * PackingMethodSettingsDefinitionDecorator constructor.
	 *
	 * @param SettingsDefinition $settings_definition .
	 * @param string $locale .
	 */
	public function __construct( SettingsDefinition $settings_definition, $locale ) {
		parent::__construct( $settings_definition, '', '', [] );
		$this->decorated_settings_definition = $settings_definition;
		$this->locale = $locale;
	}

	/**
	 * @return array[]
	 * @phpstan-ignore-next-line
	 */
	public function get_form_fields() {
		$packer_settings = new PackerSettings(
			'pl_PL' === $this->locale ? 'http://www.fedex.com/pl/shipping-services/standard-packaging.html' : 'https://www.fedex.com/en-us/service-guide/packing-express-ground/express-supplies.html'
		);

		return $packer_settings
			->add_packaging_fields(
				$this->decorated_settings_definition,
				OneRateSettingsDefinitionDecorator::OPTION_ONE_RATE
			)
			->get_form_fields();
	}

	/**
	 * Validate settings.
	 *
	 * @param SettingsValues $settings Settings values.
	 *
	 * @return bool
	 */
	public function validate_settings( SettingsValues $settings ) {
		return $this->decorated_settings_definition->validate_settings( $settings );
	}

}
