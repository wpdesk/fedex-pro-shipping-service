<?php

namespace WPDesk\FedexProShippingService;

use FedEx\RateService\ComplexType\RateReply;
use Psr\Log\LoggerInterface;
use WPDesk\AbstractShipping\Exception\InvalidSettingsException;
use WPDesk\AbstractShipping\Exception\RateException;
use WPDesk\AbstractShipping\Exception\UnitConversionException;
use WPDesk\AbstractShipping\Rate\ShipmentRating;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\AbstractShipping\Shipment\Shipment;
use WPDesk\AbstractShipping\ShippingServiceCapability\CanReturnDeliveryDate;
use WPDesk\AbstractShipping\Shop\ShopSettings;
use WPDesk\FedexProShippingService\CutoffTime\CutoffTimeSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\EstimatedDelivery\EstimatedDeliverySettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\FedexApi\FedexProRateReplyInterpretation;
use WPDesk\FedexProShippingService\FedexApi\FedexProRateRequestBuilder;
use WPDesk\FedexProShippingService\LeadTime\LeadTimeSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\MaximumTransitTime\MaximumTransitTimeRatesFilter;
use WPDesk\FedexProShippingService\MaximumTransitTime\MaximumTransitTimeSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\ShipTimestamp\ShipTimestamp;
use WPDesk\FedexShippingService\Exception\CurrencySwitcherException;
use WPDesk\FedexShippingService\FedexApi\FedexProRateCurrencyFilter;
use WPDesk\FedexShippingService\FedexApi\FedexRateReplyInterpretation;
use WPDesk\FedexShippingService\FedexApi\FedexRateRequestBuilder;
use WPDesk\FedexShippingService\FedexSettingsDefinition;
use WPDesk\FedexShippingService\FedexShippingService;

/**
 * FedEx PRO main shipping class injected into WooCommerce shipping method.
 *
 * @package WPDesk\FedexShippingService
 */
class FedexProShippingService extends FedexShippingService implements CanReturnDeliveryDate {

	/** @var ShopSettings */
	private $shop_settings;

	private $ship_timestamp;

	/**
	 * .
	 *
	 * @param LoggerInterface $logger Logger.
	 * @param ShopSettings $shop_settings Shop settings.
	 */
	public function __construct( LoggerInterface $logger, ShopSettings $shop_settings ) {
		parent::__construct( $logger, $shop_settings );
		$this->shop_settings = $shop_settings;
	}

	/**
	 * Get settings
	 *
	 * @return FedexProSettingsDefinition
	 */
	public function get_settings_definition() {
		return new FedexProSettingsDefinition( parent::get_settings_definition(), $this->shop_settings->get_locale() );
	}

	/**
	 * Verify currency.
	 *
	 * @param string $default_shop_currency Shop currency.
	 * @param string $checkout_currency Checkout currency.
	 *
	 * @return void
	 * @throws CurrencySwitcherException .
	 */
	protected function verify_currency( $default_shop_currency, $checkout_currency ) {
		// Currently do nothing. PRO version supports multicurrency.
	}

	/**
	 * Creates rate filter by currency.
	 *
	 * @param ShipmentRating $rating .
	 *
	 * @return FedexProRateCurrencyFilter .
	 */
	protected function create_filter_rates_by_currency( ShipmentRating $rating ) {
		return new FedexProRateCurrencyFilter( $rating, $this->shop_settings );
	}

	/**
	 * Create rate request builder.
	 *
	 * @param SettingsValues $settings .
	 * @param Shipment $shipment .
	 * @param ShopSettings $shop_settings .
	 *
	 * @return FedexRateRequestBuilder
	 */
	protected function create_rate_request_builder(
		SettingsValues $settings,
		Shipment $shipment,
		ShopSettings $shop_settings
	) {
		return new FedexProRateRequestBuilder( $settings, $shipment, $shop_settings, $this->create_and_get_ship_timestamp( $settings ) );
	}

	/**
	 * Create reply interpretation.
	 *
	 * @param RateReply $rate_reply .
	 * @param ShopSettings $shop_settings .
	 * @param SettingsValues $settings .
	 *
	 * @return FedexRateReplyInterpretation
	 */
	protected function create_reply_interpretation( RateReply $rate_reply, $shop_settings, $settings ) {
		return new FedexProRateReplyInterpretation(
			$rate_reply,
			$shop_settings->is_tax_enabled(),
			$settings->get_value( FedexSettingsDefinition::FIELD_REQUEST_TYPE,
				FedexSettingsDefinition::FIELD_REQUEST_TYPE_VALUE_ALL ),
			$this->create_and_get_ship_timestamp( $settings )
		);
	}

	/**
	 * Rate shipment.
	 *
	 * @param SettingsValues $settings Settings Values.
	 * @param Shipment $shipment Shipment.
	 *
	 * @return ShipmentRating
	 * @throws InvalidSettingsException InvalidSettingsException.
	 * @throws RateException RateException.
	 * @throws UnitConversionException Weight exception.
	 */
	public function rate_shipment( SettingsValues $settings, Shipment $shipment ) {
		return $this->decorate_rating_implementation_for_maximum_transit_time_if_enabled( parent::rate_shipment( $settings, $shipment ), $settings );
	}

	/**
	 * Decorate rating implementation for maximum transit time if enabled.
	 *
	 * @param ShipmentRating $shipment_rating .
	 * @param SettingsValues $settings .
	 *
	 * @return ShipmentRating|MaximumTransitTimeRatesFilter
	 */
	private function decorate_rating_implementation_for_maximum_transit_time_if_enabled(
		ShipmentRating $shipment_rating,
		SettingsValues $settings
	) {
		$delivery_dates = $settings->get_value(
			EstimatedDeliverySettingsDefinitionDecorator::OPTION_DELIVERY_DATES,
			EstimatedDeliverySettingsDefinitionDecorator::OPTION_NONE
		);

		if ( EstimatedDeliverySettingsDefinitionDecorator::OPTION_NONE !== $delivery_dates ) {
			$maximum_transit_time_setting = $settings->get_value( MaximumTransitTimeSettingsDefinitionDecorator::OPTION_MAXIMUM_TRANSIT_TIME, '' );
			if ( $maximum_transit_time_setting && is_numeric( $maximum_transit_time_setting ) ) {
				$maximum_transit_time_setting = intval( $maximum_transit_time_setting );
				$shipment_rating              = new MaximumTransitTimeRatesFilter(
					$shipment_rating,
					$maximum_transit_time_setting
				);
			}
		}
		return $shipment_rating;
	}

	/**
	 * .
	 *
	 * @param SettingsValues $settings .
	 *
	 * @return ShipTimestamp
	 */
	private function create_and_get_ship_timestamp( SettingsValues $settings ) {
		if ( null === $this->ship_timestamp ) {
			$this->ship_timestamp = ShipTimestamp::create_from_settings( $settings );
		}
		return $this->ship_timestamp;
	}

}

