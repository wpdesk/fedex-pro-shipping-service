<?php
/**
 * Decorator for cutoff time.
 *
 * @package WPDesk\FedexProShippingService\CutoffTime
 */

namespace WPDesk\FedexProShippingService\CutoffTime;

use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierAfter;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\FedexProShippingService\LeadTime\LeadTimeSettingsDefinitionDecorator;

/**
 * Can decorate settings for lead time field.
 */
class CutoffTimeSettingsDefinitionDecorator extends SettingsDefinitionModifierAfter {
	const OPTION_CUTOFF_TIME = 'cutoff_time';

	public function __construct( SettingsDefinition $fedex_settings_definition ) {
		parent::__construct( $fedex_settings_definition, LeadTimeSettingsDefinitionDecorator::OPTION_LEAD_TIME, self::OPTION_CUTOFF_TIME, array(
			'title'       => \__( 'Cutoff Time', 'fedex-pro-shipping-service' ),
			'type'        => 'select',
			'description' => \__( 'Cutoff Time is used to define what time you stop preparing orders same day. Cutoff time is only used if your lead time is set to zero.', 'fedex-pro-shipping-service' ),
			'desc_tip'    => \true,
			'options'     => array(
				''   => \__( 'None', 'fedex-pro-shipping-service' ),
				'1'  => \__( '1 AM', 'fedex-pro-shipping-service' ),
				'2'  => \__( '2 AM', 'fedex-pro-shipping-service' ),
				'3'  => \__( '3 AM', 'fedex-pro-shipping-service' ),
				'4'  => \__( '4 AM', 'fedex-pro-shipping-service' ),
				'5'  => \__( '5 AM', 'fedex-pro-shipping-service' ),
				'6'  => \__( '6 AM', 'fedex-pro-shipping-service' ),
				'7'  => \__( '7 AM', 'fedex-pro-shipping-service' ),
				'8'  => \__( '8 AM', 'fedex-pro-shipping-service' ),
				'9'  => \__( '9 AM', 'fedex-pro-shipping-service' ),
				'10' => \__( '10 AM', 'fedex-pro-shipping-service' ),
				'11' => \__( '11 AM', 'fedex-pro-shipping-service' ),
				'12' => \__( '12 PM', 'fedex-pro-shipping-service' ),
				'13' => \__( '1 PM', 'fedex-pro-shipping-service' ),
				'14' => \__( '2 PM', 'fedex-pro-shipping-service' ),
				'15' => \__( '3 PM', 'fedex-pro-shipping-service' ),
				'16' => \__( '4 PM', 'fedex-pro-shipping-service' ),
				'17' => \__( '5 PM', 'fedex-pro-shipping-service' ),
				'18' => \__( '6 PM', 'fedex-pro-shipping-service' ),
				'19' => \__( '7 PM', 'fedex-pro-shipping-service' ),
				'20' => \__( '8 PM', 'fedex-pro-shipping-service' ),
				'21' => \__( '9 PM', 'fedex-pro-shipping-service' ),
				'22' => \__( '10 PM', 'fedex-pro-shipping-service' ),
				'23' => \__( '11 PM', 'fedex-pro-shipping-service' )
			)
		) );
	}
}
