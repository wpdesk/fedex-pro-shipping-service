<?php
/**
 * Decorator for handling fees settings field.
 *
 * @package WPDesk\FedexProShippingService\HandlingFees
 */

namespace WPDesk\FedexProShippingService\HandlingFees;

use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierAfter;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\FedexShippingService\FedexSettingsDefinition;

/**
 * Can decorate settings by adding handling fees field.
 */
class HandlingFeesSettingsDefinitionDecorator extends SettingsDefinitionModifierAfter {

	const HANDLING_FEES = 'handling_fees';

	const FIELD_TYPE = 'handling_fees';

	public function __construct( SettingsDefinition $fedex_settings_definition ) {
		parent::__construct(
			$fedex_settings_definition,
			FedexSettingsDefinition::FIELD_REQUEST_TYPE,
			self::HANDLING_FEES,
			array(
				'type' => self::FIELD_TYPE,
			)
		);
	}

}