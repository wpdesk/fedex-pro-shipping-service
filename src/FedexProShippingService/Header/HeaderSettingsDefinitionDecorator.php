<?php
/**
 * Decorator for shipping zone header.
 *
 * @package WPDesk\FedexProShippingService\CustomOrigin
 */

namespace WPDesk\FedexProShippingService\CustomOrigin;

use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierAfter;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\FedexShippingService\FedexSettingsDefinition;

/**
 * Can decorate settings by adding shipping zone header.
 */
class HeaderSettingsDefinitionDecorator extends SettingsDefinitionModifierAfter {

	const FEDEX_HEADER_SHIPPING_ZONE = 'fedex_header_shipping_zone';

	/**
	 * @param SettingsDefinition $fedex_settings_definition .
	 * @param string $locale .
	 */
	public function __construct( SettingsDefinition $fedex_settings_definition, $locale ) {
		$is_pl     = 'pl_PL' === $locale;
		$docs_link = $is_pl ? 'https://octol.io/fedex-settings-docs-pl' : 'https://octol.io/fedex-setting-docs';

		parent::__construct(
			$fedex_settings_definition,
			FedexSettingsDefinition::FEDEX_HEADER,
			self::FEDEX_HEADER_SHIPPING_ZONE,
			[
				'title'       => __( 'Method Settings', 'flexible-shipping-dhl-express-pro' ),
				'type'        => 'title',
				'description' => __( 'Set how FedEx services are displayed.', 'flexible-shipping-dhl-express-pro' ),
			]
		);
	}

}
