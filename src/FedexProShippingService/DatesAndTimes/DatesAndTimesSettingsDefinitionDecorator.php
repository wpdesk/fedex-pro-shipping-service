<?php

/**
 * Decorator for dates and times section.
 *
 * @package WPDesk\FedexProShippingService\DatesAndTimes
 */
namespace WPDesk\FedexProShippingService\DatesAndTimes;

use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierAfter;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\FedexShippingService\FedexSettingsDefinition;

/**
 * Can decorate settings by adding handling fees field.
 */
class DatesAndTimesSettingsDefinitionDecorator extends SettingsDefinitionModifierAfter {

	const DATES_AND_TIMES_TITLE = 'dates_and_times_title';

	public function __construct( SettingsDefinition $fedex_settings_definition ) {
		parent::__construct( $fedex_settings_definition, FedexSettingsDefinition::FIELD_DESTINATION_ADDRESS_TYPE, self::DATES_AND_TIMES_TITLE, array(
				'title'       => \__( 'Dates & Time', 'fedex-pro-shipping-service' ),
				'description' => \__( 'Manage services\' dates information.', 'fedex-pro-shipping-service' ),
				'type'        => 'title'
			)
		);
	}
}
