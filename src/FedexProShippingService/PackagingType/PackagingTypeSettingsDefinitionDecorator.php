<?php
/**
 * Decorator for packaging type.
 *
 * @package WPDesk\FedexProShippingService\PackagingType
 */

namespace WPDesk\FedexProShippingService\PackagingType;

use FedEx\OpenShipService\SimpleType\PackagingType;
use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierAfter;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\FedexShippingService\FedexSettingsDefinition;

/**
 * Can decorate settings for packaging type field.
 */
class PackagingTypeSettingsDefinitionDecorator extends SettingsDefinitionModifierAfter {

	const OPTION_PACKAGING_TYPE = 'packaging_type';

	const OPTION_PACKAGING_TYPE_DEFAULT = PackagingType::_YOUR_PACKAGING;

	public function __construct( SettingsDefinition $fedex_settings_definition ) {
		parent::__construct(
			$fedex_settings_definition,
			FedexSettingsDefinition::FIELD_DESTINATION_ADDRESS_TYPE,
			self::OPTION_PACKAGING_TYPE,
			array(
				'title'       => __( 'Packaging Type', 'fedex-pro-shipping-service' ),
				'type'        => 'select',
				'description' => sprintf(
					__( 'Choose the type of packaging which will be used to ship the ordered products. Please mind that the FedEx One Rate pricing requires the FEDEX_ default packagings to be used. Learn more about FedEx default packaging types %1$shere →%2$s', 'fedex-pro-shipping-service' ),
					'<a target="_blank" href="https://www.fedex.com/us/developer/webhelp/ws/2020/US/FedEx_WebServices_2020_Developer_Guide.htm#t=wsdvg%2FHow_to_Specify_One_Rate_Pricing.htm">',
					'</a>'
				),
				'desc_tip'    => false,
				'default'     => self::OPTION_PACKAGING_TYPE_DEFAULT,
				'options'     => [
					PackagingType::_YOUR_PACKAGING => PackagingType::_YOUR_PACKAGING,
					PackagingType::_FEDEX_ENVELOPE => PackagingType::_FEDEX_ENVELOPE,
					PackagingType::_FEDEX_PAK => PackagingType::_FEDEX_PAK,
					PackagingType::_FEDEX_TUBE => PackagingType::_FEDEX_TUBE,
					PackagingType::_FEDEX_BOX => PackagingType::_FEDEX_BOX,
					PackagingType::_FEDEX_SMALL_BOX => PackagingType::_FEDEX_SMALL_BOX,
					PackagingType::_FEDEX_MEDIUM_BOX => PackagingType::_FEDEX_MEDIUM_BOX,
					PackagingType::_FEDEX_LARGE_BOX => PackagingType::_FEDEX_LARGE_BOX,
					PackagingType::_FEDEX_EXTRA_LARGE_BOX => PackagingType::_FEDEX_EXTRA_LARGE_BOX,
					PackagingType::_FEDEX_10KG_BOX => PackagingType::_FEDEX_10KG_BOX,
					PackagingType::_FEDEX_25KG_BOX => PackagingType::_FEDEX_25KG_BOX,
				],
			)
		);
	}

}
