<?php
/**
 * Request modifier for Packaging Type.
 *
 * @package WPDesk\FedexProShippingService\PackagingType
 */

namespace WPDesk\FedexProShippingService\PackagingType;

use FedEx\RateService\ComplexType\RateRequest;
use FedEx\RateService\SimpleType\RateRequestType;
use FedEx\RateService\SimpleType\ServiceOptionType;
use WPDesk\FedexProShippingService\FedexApi\FedexRateRequestModifier;
use WPDesk\FedexShippingService\FedexApi\FedexRequestManipulation;

/**
 * Can modify request for packaging type.
 */
class PackagingTypeRequestModifier implements FedexRateRequestModifier {

	/**
	 * Packaging type setting.
	 *
	 * @var string
	 */
	private $packaging_type;

	/**
	 * @var string
	 */
	private $packing_method;

	/**
	 * .
	 *
	 * @param string $packaging_type .
	 * @param string $packing_method .
	 */
	public function __construct( $packaging_type, $packing_method ) {
		$this->packaging_type = $packaging_type;
		$this->packing_method = $packing_method;
	}

	/**
	 * Modify rate request.
	 *
	 * @param RateRequest $request
	 */
	public function modify_rate_request( RateRequest $request ) {
		if ( in_array( $this->packing_method, array( 'weight', 'separately' ), true ) ) {
			$request->RequestedShipment->PackagingType = $this->packaging_type;
		}
	}

}
